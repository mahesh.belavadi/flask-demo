#!/bin/bash
# script to setup runner on the host
# TODO: Remove all hardcoaded paths 

#Setup docker
sudo apt-get update
sudo apt-get -y remove docker docker-engine docker.io
sudo apt install -y docker.io
echo "{ \"insecure-registries\":[\"192.168.1.17:5000\"] }" | sudo tee -a /etc/docker/daemon.json
sudo systemctl restart docker
sudo systemctl enable docker

#Setup docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

sudo docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest

sudo docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    --volumes-from gitlab-runner-config \
    gitlab/gitlab-runner:latest
