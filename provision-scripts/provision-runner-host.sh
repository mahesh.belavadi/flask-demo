#!/bin/bash
# script to setup runner on the host
# TODO: Remove all hardcoaded paths 
#Setup docker
sudo apt-get update
sudo apt-get -y remove docker docker-engine docker.io
sudo apt install -y docker.io
echo "{ \"insecure-registries\":[\"192.168.1.17:5000\"] }" | sudo tee -a /etc/docker/daemon.json
sudo systemctl restart docker
sudo systemctl enable docker

#Setup docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "-YXZZ8yDosyzsCtDztFx" \
  --executor "shell" \
  --description "Build server" \
  --tag-list "buildserver" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"
  
echo 'gitlab-runner ALL=(ALL) NOPASSWD: ALL' | sudo tee -a /etc/sudoers