from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hey, Welcome to flask-demo!! Stay safe...Maintain social distance :) V3'

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5030)
