sudo apt-get update
sudo apt-get -y remove docker docker-engine docker.io
sudo apt install -y docker.io
echo "{ \"insecure-registries\":[\"192.168.1.17:5000\"] }" | sudo tee -a /etc/docker/daemon.json
sudo systemctl restart docker
sudo systemctl enable docker
